import 'package:flutter/material.dart';
import 'package:flutter_pokemon/src/pokemon_model.dart';
import 'package:progress_dialog/progress_dialog.dart';

class Pokemon3 extends StatefulWidget {
  @override
  _Pokemon3State createState() => _Pokemon3State();
}

class _Pokemon3State extends State<Pokemon3> {

  List<PokemonModel> listPokemon = [];

  List<PokemonModel> listData = [
    PokemonModel('Bullbasaur', 'images/bullbasaur.png'),
    PokemonModel('Charmander', 'images/charmander.png'),
    PokemonModel('Eevee', 'images/eevee.png'),
    PokemonModel('Pidgey', 'images/pidgey.png'),
    PokemonModel('Pikachu', 'images/pikachu.png'),
    PokemonModel('Pokeball', 'images/pokeball.png'),
    PokemonModel('Rattata', 'images/rattata.png'),
    PokemonModel('Snorlax', 'images/snorlax.png'),
    PokemonModel('Squirtle', 'images/squirtle.png'),
    PokemonModel('Zubat', 'images/zubat.png'),
  ];

  var countPokemon = 0;
  addPokemon(){
    if (countPokemon < 10) {
      setState(() {
        listPokemon.add( listData[ countPokemon ] );
        countPokemon ++;
      });
    }
  }

  loadData() {
    ProgressDialog progressDialog = ProgressDialog(context);
    progressDialog.style(message: "Pokemon loading..");
    progressDialog.show();
    Future.delayed(Duration(seconds: 5), () {
      setState(() {
        listPokemon =  listData;
        progressDialog.hide();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Pokemon'),),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          // addPokemon();
          loadData();
        },
      ),
      body: ListView.builder (
        itemCount: listPokemon.length,
        itemBuilder: (context, position) {
          return Container(
            padding: EdgeInsets.all(20),
            child: Row (
              children: [
                Container (
                  width: 100,
                  padding: EdgeInsets.only(right: 20),
                  child: Image.asset( listPokemon[position].image ),
                ),
                SizedBox(
                    child: Text( listPokemon[position].name, style: TextStyle(fontSize: 20), )
                )
              ],
            ),
          );
        },
      ),
    );
  }

}
