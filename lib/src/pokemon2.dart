import 'package:flutter/material.dart';
import 'package:flutter_pokemon/src/pokemon_model.dart';


class Pokemon2 extends StatelessWidget {

  List<PokemonModel> listPokemon = [
    PokemonModel('Bullbasaur', 'images/bullbasaur.png'),
    PokemonModel('Charmander', 'images/charmander.png'),
    PokemonModel('Eevee', 'images/eevee.png'),
    PokemonModel('Pidgey', 'images/pidgey.png'),
    PokemonModel('Pikachu', 'images/pikachu.png'),
    PokemonModel('Pokeball', 'images/pokeball.png'),
    PokemonModel('Rattata', 'images/rattata.png'),
    PokemonModel('Snorlax', 'images/snorlax.png'),
    PokemonModel('Squirtle', 'images/squirtle.png'),
    PokemonModel('Zubat', 'images/zubat.png'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Pokemon'),),
      body: ListView.builder (
        itemCount: listPokemon.length,
        itemBuilder: (context, position) {
          return Container(
            padding: EdgeInsets.all(20),
            child: Row (
              children: [
                Container (
                  width: 100,
                  padding: EdgeInsets.only(right: 20),
                  child: Image.asset( listPokemon[position].image ),
                ),
                SizedBox(
                  child: Text( listPokemon[position].name, style: TextStyle(fontSize: 20), )
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
