import 'package:flutter/material.dart';


class Pokemon extends StatelessWidget {

  List<String> listPokemon = [
    'Bullbasaur',
    'Charmander',
    'Eevee',
    'Pidgey',
    'Pikachu',
    'Pokeball',
    'Rattata',
    'Snorlax',
    'Squirtle',
    'Zubat',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Pokemon'),),
      body: ListView.builder (
        itemCount: listPokemon.length,
        itemBuilder: (context, position) {
          return Container(
            padding: EdgeInsets.all(20),
            child: Text( listPokemon[position], style: TextStyle(fontSize: 20))
          );
        },
      ),
    );
  }
}
